# script created during/for a class
# keep none authorized remote connections killed
# make sure critical processes/services "auto restart"

# powershell -ExecutionPolicy bypass -Force -file authRemoteOnly.ps1
$arrSafeIPs = @(
    "192.168.10.38",
    #"192.168.10.55",
    "192.168.10.21", 
    "192.168.10.45", 
    "192.168.10.110",
	"77.234.42.23"  
)
   
while($true)
{
	write-host "---------------------------------"
	$rawOutput = (cmd /c "for /F ""tokens=1-5 delims= "" %A in ('netstat -ano') do echo %A,%B,%C,%D,%E")
	ForEach ($line in $rawOutput ){
		If ($line -Match "ESTABLISHED") {
			write-host "found line: " $line
			$col = $line.split(",")
			$currentIP = $col[2].split(":")[0]
			write-host "remote:" $currentIP
			If ($arrSafeIPs -Contains $currentIP) {
				write-host "it's safe"
				Continue
			}  
			write-host "process:" $col[4]
			stop-process -id $col[4] -Force
		}
	}
}



# Function Get-ActiveTCPConnections {            
# [cmdletbinding()]            
# param(            
# )            
            
# try {            
    # $TCPProperties = [System.Net.NetworkInformation.IPGlobalProperties]::GetIPGlobalProperties()            
    # $Connections = $TCPProperties.GetActiveTcpConnections()            
    # foreach($Connection in $Connections) {            
        # if($Connection.LocalEndPoint.AddressFamily -eq "InterNetwork" ) { $IPType = "IPv4" } else { $IPType = "IPv6" }            
        # $OutputObj = New-Object -TypeName PSobject            
        # #$OutputObj | Add-Member -MemberType NoteProperty -Name "LocalAddress" -Value $Connection.LocalEndPoint.Address            
        # #$OutputObj | Add-Member -MemberType NoteProperty -Name "LocalPort" -Value $Connection.LocalEndPoint.Port            
        # $OutputObj | Add-Member -MemberType NoteProperty -Name "RemoteAddress" -Value $Connection.RemoteEndPoint.Address  
        # If ($arrSafeIPs -Contains $Connection.RemoteEndPoint.Address) {
            # Continue
        # }
        # #$OutputObj | Add-Member -MemberType NoteProperty -Name "RemotePort" -Value $Connection.RemoteEndPoint.Port            
        # #$OutputObj | Add-Member -MemberType NoteProperty -Name "State" -Value $Connection.State            
        # #$OutputObj | Add-Member -MemberType NoteProperty -Name "IPV4Or6" -Value $IPType            
        # $OutputObj            
    # }            
            
# } catch {            
    # Write-Error "Failed to get active connections. $_"            
# }           
# }

#Get-ActiveTCPConnections | ? {$_.RemoteAddress}